package app.fedilab.fedilabtube.activities;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import app.fedilab.fedilabtube.BuildConfig;
import app.fedilab.fedilabtube.R;
import app.fedilab.fedilabtube.helper.Helper;
import app.fedilab.fedilabtube.helper.HelperInstance;
import app.fedilab.fedilabtube.helper.Theme;


public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Theme.setTheme(this, HelperInstance.getLiveInstance(this),false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView about_version = findViewById(R.id.about_version);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            about_version.setText(getResources().getString(R.string.about_vesrion, version));
        } catch (PackageManager.NameNotFoundException ignored) {
        }

        setTitle(R.string.about_the_app);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //Developer click for Mastodon account
        TextView developer_mastodon = findViewById(R.id.developer_mastodon);
        SpannableString content = new SpannableString(developer_mastodon.getText().toString());
        content.setSpan(new ForegroundColorSpan(Helper.fetchAccentColor(AboutActivity.this)), 0, content.length(), 0);
        developer_mastodon.setText(content);
        developer_mastodon.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://toot.fedilab.app/@apps"));
            startActivity(browserIntent);
        });

        Button translation = findViewById(R.id.translation);
        if (BuildConfig.full_instances) {
            translation.setVisibility(View.VISIBLE);
        } else {
            translation.setVisibility(View.GONE);
        }
        translation.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://crowdin.com/project/tubelab"));
            startActivity(browserIntent);
        });

        //App Name:
        TextView app_name = findViewById(R.id.app_name);
        app_name.setText(R.string.app_name);


        //Developer Framagit
        TextView framagit = findViewById(R.id.framagit);
        content = new SpannableString(framagit.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        framagit.setText(content);
        framagit.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://framagit.org/tom79"));
            startActivity(browserIntent);
        });


        LinearLayout donation_container = findViewById(R.id.donation_container);
        if (BuildConfig.google_restriction || !BuildConfig.full_instances) {
            donation_container.setVisibility(View.GONE);
        }


        //Developer donation
        Button donatePaypal = findViewById(R.id.donate_paypal);
        donatePaypal.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.paypal.me/Mastalab"));
            startActivity(browserIntent);
        });
        Button donateLiberapay = findViewById(R.id.donate_liberapay);
        donateLiberapay.setOnClickListener(v -> {
            Intent browserIntent;
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://liberapay.com/tom79"));

            startActivity(browserIntent);
        });


        TextView license = findViewById(R.id.license);
        content = new SpannableString(license.getText().toString());
        content.setSpan(new ForegroundColorSpan(Helper.fetchAccentColor(AboutActivity.this)), 0, content.length(), 0);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        license.setText(content);
        license.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gnu.org/licenses/quick-guide-gplv3.fr.html"));
            startActivity(browserIntent);
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
