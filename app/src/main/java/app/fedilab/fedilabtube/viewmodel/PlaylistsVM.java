package app.fedilab.fedilabtube.viewmodel;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import app.fedilab.fedilabtube.client.APIResponse;
import app.fedilab.fedilabtube.client.RetrofitPeertubeAPI;
import app.fedilab.fedilabtube.client.data.AccountData.Account;
import app.fedilab.fedilabtube.client.data.PlaylistData.Playlist;
import app.fedilab.fedilabtube.client.data.VideoPlaylistData;
import app.fedilab.fedilabtube.helper.Helper;
import app.fedilab.fedilabtube.sqlite.AccountDAO;
import app.fedilab.fedilabtube.sqlite.ManagePlaylistsDAO;
import app.fedilab.fedilabtube.sqlite.Sqlite;


public class PlaylistsVM extends AndroidViewModel {
    private MutableLiveData<APIResponse> apiResponseMutableLiveData;
    private MutableLiveData<List<VideoPlaylistData.VideoPlaylistExport>> videoPlaylistExportMutableLiveData;

    public PlaylistsVM(@NonNull Application application) {
        super(application);
    }

    public LiveData<APIResponse> manage(action apiAction, Playlist playlist, String videoId) {
        apiResponseMutableLiveData = new MutableLiveData<>();
        managePlaylists(apiAction, playlist, videoId);
        return apiResponseMutableLiveData;
    }

    public LiveData<List<VideoPlaylistData.VideoPlaylistExport>> localePlaylist() {
        videoPlaylistExportMutableLiveData = new MutableLiveData<>();
        loadLocalePlaylist();
        return videoPlaylistExportMutableLiveData;
    }


    public LiveData<APIResponse> videoExists(List<String> videoIds) {
        apiResponseMutableLiveData = new MutableLiveData<>();
        checkVideosExist(videoIds);
        return apiResponseMutableLiveData;
    }

    private void checkVideosExist(List<String> videoIds) {
        Context _mContext = getApplication().getApplicationContext();
        new Thread(() -> {
            APIResponse apiResponse = new RetrofitPeertubeAPI(_mContext).getVideosExist(videoIds);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> apiResponseMutableLiveData.setValue(apiResponse);
            mainHandler.post(myRunnable);
        }).start();
    }


    private void loadLocalePlaylist() {
        Context _mContext = getApplication().getApplicationContext();
        new Thread(() -> {
            try {
                SQLiteDatabase db = Sqlite.getInstance(_mContext.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                List<VideoPlaylistData.VideoPlaylistExport> videoPlaylistExports = new ManagePlaylistsDAO(_mContext, db).getAllPlaylists();
                Handler mainHandler = new Handler(Looper.getMainLooper());
                Runnable myRunnable = () -> videoPlaylistExportMutableLiveData.setValue(videoPlaylistExports);
                mainHandler.post(myRunnable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void managePlaylists(action apiAction, Playlist playlist, String videoId) {
        Context _mContext = getApplication().getApplicationContext();
        new Thread(() -> {
            try {
                String token = Helper.getToken(_mContext);
                SQLiteDatabase db = Sqlite.getInstance(_mContext.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                Account account = new AccountDAO(_mContext, db).getAccountByToken(token);
                int statusCode = -1;
                APIResponse apiResponse;
                if (account == null) {
                    statusCode = 403;
                    apiResponse = new APIResponse();
                    apiResponse.setPlaylists(new ArrayList<>());
                } else {
                    apiResponse = new RetrofitPeertubeAPI(_mContext).playlistAction(apiAction, playlist != null ? playlist.getId() : null, videoId, account.getAcct(), null);
                }
                Handler mainHandler = new Handler(Looper.getMainLooper());
                if (apiResponse != null) {
                    apiResponse.setStatusCode(statusCode);
                }
                APIResponse finalApiResponse = apiResponse;
                Runnable myRunnable = () -> apiResponseMutableLiveData.setValue(finalApiResponse);
                mainHandler.post(myRunnable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public enum action {
        GET_PLAYLISTS,
        GET_PLAYLIST_INFO,
        GET_LIST_VIDEOS,
        CREATE_PLAYLIST,
        UPDATE_PLAYLIST,
        DELETE_PLAYLIST,
        ADD_VIDEOS,
        DELETE_VIDEOS
    }
}
