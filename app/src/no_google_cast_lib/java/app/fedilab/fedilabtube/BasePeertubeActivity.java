package app.fedilab.fedilabtube;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.exoplayer2.SimpleExoPlayer;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;

import app.fedilab.fedilabtube.client.data.VideoData;
import app.fedilab.fedilabtube.databinding.ActivityPeertubeBinding;
import app.fedilab.fedilabtube.helper.Helper;
import su.litvak.chromecast.api.v2.ChromeCast;
import su.litvak.chromecast.api.v2.MediaStatus;
import su.litvak.chromecast.api.v2.Status;

import static app.fedilab.fedilabtube.helper.Helper.CAST_ID;

/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

public class BasePeertubeActivity extends AppCompatActivity {

    protected ActivityPeertubeBinding binding;
    protected VideoData.Video peertube;
    protected SimpleExoPlayer player;
    protected String videoURL;
    protected String subtitlesStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPeertubeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.minController.castPlay.setOnClickListener(v -> {
            binding.minController.castLoader.setVisibility(View.VISIBLE);
            if (BaseMainActivity.chromeCast != null) {
                new Thread(() -> {
                    try {
                        int icon = -1;
                        if (BaseMainActivity.chromeCast.getMediaStatus().playerState == MediaStatus.PlayerState.PLAYING) {
                            BaseMainActivity.chromeCast.pause();
                            icon = R.drawable.ic_baseline_play_arrow_32;
                        } else if (BaseMainActivity.chromeCast.getMediaStatus().playerState == MediaStatus.PlayerState.PAUSED) {
                            BaseMainActivity.chromeCast.play();
                            icon = R.drawable.ic_baseline_pause_32;
                        }
                        if (icon != -1) {
                            Handler mainHandler = new Handler(Looper.getMainLooper());
                            int finalIcon = icon;
                            Runnable myRunnable = () -> binding.minController.castPlay.setImageResource(finalIcon);
                            mainHandler.post(myRunnable);
                        }
                        Handler mainHandler = new Handler(Looper.getMainLooper());
                        Runnable myRunnable = () -> binding.minController.castLoader.setVisibility(View.GONE);
                        mainHandler.post(myRunnable);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cast) {
            if (BaseMainActivity.chromeCasts != null && BaseMainActivity.chromeCasts.size() > 0) {
                String[] chromecast_choice = new String[BaseMainActivity.chromeCasts.size()];
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
                alt_bld.setTitle(R.string.chromecast_choice);
                int i = 0;
                for (ChromeCast cc : BaseMainActivity.chromeCasts) {
                    chromecast_choice[i] = cc.getTitle();
                    i++;
                }
                i = 0;
                for (ChromeCast cc : BaseMainActivity.chromeCasts) {
                    if (BaseMainActivity.chromecastActivated && cc.isConnected()) {
                        break;
                    }
                    i++;
                }

                alt_bld.setSingleChoiceItems(chromecast_choice, i, (dialog, position) -> {
                    BaseMainActivity.chromeCast = BaseMainActivity.chromeCasts.get(position);
                    new Thread(() -> {
                        if (BaseMainActivity.chromeCast != null) {
                            Intent intentBC = new Intent(Helper.RECEIVE_CAST_SETTINGS);
                            Bundle b = new Bundle();
                            if (BaseMainActivity.chromecastActivated) {
                                b.putInt("displayed", 0);
                                intentBC.putExtras(b);
                                LocalBroadcastManager.getInstance(BasePeertubeActivity.this).sendBroadcast(intentBC);
                                Handler mainHandler = new Handler(Looper.getMainLooper());
                                Runnable myRunnable = () -> {
                                    binding.doubleTapPlayerView.setVisibility(View.VISIBLE);
                                    binding.minController.castMiniController.setVisibility(View.GONE);
                                };
                                mainHandler.post(myRunnable);

                            } else {
                                b.putInt("displayed", 1);
                                b.putParcelable("castedTube", peertube);
                                intentBC.putExtras(b);
                                LocalBroadcastManager.getInstance(BasePeertubeActivity.this).sendBroadcast(intentBC);
                                try {
                                    Handler mainHandler = new Handler(Looper.getMainLooper());
                                    Runnable myRunnable = () -> {
                                        invalidateOptionsMenu();
                                        binding.minController.castLoader.setVisibility(View.VISIBLE);
                                        player.setPlayWhenReady(false);
                                        binding.doubleTapPlayerView.setVisibility(View.GONE);
                                        binding.minController.castMiniController.setVisibility(View.VISIBLE);
                                        dialog.dismiss();
                                        if (videoURL != null) {
                                            if (player != null && player.getCurrentPosition() > 0) {
                                                videoURL += "?start=" + (player.getCurrentPosition() / 1000);
                                            }
                                        }
                                    };
                                    mainHandler.post(myRunnable);
                                    if (!BaseMainActivity.chromeCast.isConnected()) {
                                        BaseMainActivity.chromeCast.connect();
                                    }
                                    myRunnable = this::invalidateOptionsMenu;
                                    mainHandler.post(myRunnable);
                                    Status status = BaseMainActivity.chromeCast.getStatus();
                                    if (BaseMainActivity.chromeCast.isAppAvailable(CAST_ID) && !status.isAppRunning(CAST_ID)) {
                                        BaseMainActivity.chromeCast.launchApp(CAST_ID);
                                    }
                                    if (videoURL != null) {
                                        String mime = MimeTypeMap.getFileExtensionFromUrl(videoURL);
                                        BaseMainActivity.chromeCast.setRequestTimeout(60000);
                                        BaseMainActivity.chromeCast.load(peertube.getTitle(), null, videoURL, mime);
                                        BaseMainActivity.chromeCast.play();
                                        binding.minController.castPlay.setImageResource(R.drawable.ic_baseline_pause_32);
                                    }
                                    myRunnable = () -> binding.minController.castLoader.setVisibility(View.GONE);
                                    mainHandler.post(myRunnable);
                                } catch (IOException | GeneralSecurityException e) {
                                    e.printStackTrace();
                                }
                            }
                            Handler mainHandler = new Handler(Looper.getMainLooper());
                            Runnable myRunnable = () -> {
                                invalidateOptionsMenu();
                                dialog.dismiss();
                            };
                            mainHandler.post(myRunnable);
                        }
                    }).start();
                });
                alt_bld.setPositiveButton(R.string.close, (dialog, id) -> dialog.dismiss());
                AlertDialog alert = alt_bld.create();
                alert.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        getMenuInflater().inflate(R.menu.video_menu, menu);
        MenuItem castItem = menu.findItem(R.id.action_cast);
        if (BaseMainActivity.chromeCasts != null && BaseMainActivity.chromeCasts.size() > 0) {
            castItem.setVisible(true);
            if (BaseMainActivity.chromeCast != null && BaseMainActivity.chromeCast.isConnected()) {
                castItem.setIcon(R.drawable.ic_baseline_cast_connected_24);
            } else {
                castItem.setIcon(R.drawable.ic_baseline_cast_24);
            }
        }
        return true;
    }

}
