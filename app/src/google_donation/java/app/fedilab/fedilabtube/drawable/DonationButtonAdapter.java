package app.fedilab.fedilabtube.drawable;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.SkuDetails;

import java.util.List;
import java.util.Locale;

import app.fedilab.fedilabtube.DonationActivity;
import app.fedilab.fedilabtube.R;
import app.fedilab.fedilabtube.databinding.DrawerDonationBinding;


public class DonationButtonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<SkuDetails> skuDetails;
    private final BillingClient billingClient;
    private Context context;
    private final boolean isSubscription;

    public DonationButtonAdapter(List<SkuDetails> skuDetails, BillingClient billingClient, boolean subscription) {
        this.isSubscription = subscription;
        this.skuDetails = skuDetails;
        this.billingClient = billingClient;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        DrawerDonationBinding itemBinding = DrawerDonationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolder holder = (ViewHolder) viewHolder;
        SkuDetails skuDetail = skuDetails.get(position);
        String currency = skuDetail.getPriceCurrencyCode();
        String price = skuDetail.getPrice();
        if (isSubscription) {
            holder.binding.buttonDonation.setText(String.format(Locale.getDefault(), "%s %s / %s", price, currency, context.getString(R.string.month)));
        } else {
            holder.binding.buttonDonation.setText(String.format(Locale.getDefault(), "%s %s", price, currency));
        }

        holder.binding.buttonDonation.setOnClickListener(v -> {
            BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetail)
                    .build();
            billingClient.launchBillingFlow((DonationActivity) context, billingFlowParams);
        });
    }


    @Override
    public int getItemCount() {
        return skuDetails.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        DrawerDonationBinding binding;

        ViewHolder(DrawerDonationBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }
}
